FROM python:3-alpine
LABEL maintainer="ToM  <tom@leloop.org>"


# hadolint ignore=DL3018
RUN apk add --no-cache \
		git \
		graphviz \
		ttf-freefont

WORKDIR /src
RUN wget --quiet https://raw.githubusercontent.com/esc/git-big-picture/master/git-big-picture  \
		-O /usr/local/bin/git-big-picture \
	&& chmod +x /usr/local/bin/git-big-picture

CMD ["/usr/local/bin/git-big-picture"]
