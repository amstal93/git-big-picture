# git-big-picture

Docker image for [esc/git-big-picture](https://github.com/esc/git-big-picture).

## Usage

The image `WORKINGDIR` is `/src`, so you need to bind-mount your git
repository there:

```bash
docker run --rm -v $PWD:/src -u $(id -u):$(id -g) letompouce/git-big-picture -o repository.png
```

## GitLab CI usage

```yaml
git-big-picture:
  stage: docs
  image: letompouce/git-big-picture
  before_script:
    - git-big-picture --version
  script:
    - git-big-picture -o repository.png
  artifacts:
    paths:
      - repository.png
    expire_in: one week
```
